<?php


interface ParserInterface
{
    /**
     * 
     * @param string $url
     * @param string $taf
     * @return array
     */
    public function process(string $url, string $taf): array;
}
